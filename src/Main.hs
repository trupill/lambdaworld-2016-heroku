{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric     #-}
module Main where

import Data.Aeson
import Data.List
import Data.Time
import GHC.Generics
import Network.Wai.Handler.Warp
import Servant
import System.Environment

data List = List { listId     :: Integer
                 , listName   :: String
                 , listBefore :: Maybe UTCTime }
          deriving (Show, Generic)
instance ToJSON List

data ListItem = ListItem { listItemText :: String
                         , listItemDone :: Bool }
              deriving (Show, Generic)
instance ToJSON ListItem

exampleLists :: [(List, [ListItem])]
exampleLists = [ ( List 1 "Workshop" Nothing
                 , [ListItem "Servant" True, ListItem "Heroku" False] )
               , ( List 2 "Shopping" Nothing
                 , [ListItem "Cheese" True, ListItem "Morcilla" False] )
               ]

type API = "lists" :> Get '[JSON] [List]
      :<|> "list"  :> Capture "id" Integer
                   :> Get '[JSON] List
      :<|> "list"  :> Capture "id" Integer
                   :> "items"
                   :> Get '[JSON] [ListItem]

srv :: Server API
srv = lists :<|> list :<|> listItems
  where lists :: Handler [List]
        lists = return (map fst exampleLists)
        findList :: Integer -> Maybe (List, [ListItem])
        findList id_ = find (\(l,_) -> listId l == id_) exampleLists
        list :: Integer -> Handler List
        list id_ = 
          do case findList id_ of
               Nothing    -> throwError err404
               Just (l,_) -> return l
        listItems :: Integer -> Handler [ListItem]
        listItems id_ =
          do case findList id_ of
               Nothing    -> throwError err404
               Just (_,i) -> return i


main :: IO ()
main = do putStrLn "Starting app"
          port <- read <$> getEnv "PORT"
          putStrLn $ "Listening on port " ++ show port
          run port $ serve (Proxy :: Proxy API) srv
